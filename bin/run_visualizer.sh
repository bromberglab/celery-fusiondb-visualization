#!/usr/bin/env bash

cd "$(dirname "$0")"
java -Djava.awt.headless=true -jar visualizer.jar -q $1 -t $2 -i $3 -o $4 -c $5 -l $6 -s $7 -n $8 -e $9 ${10} -r size -d metadata_map.properties

import os
from celery import Celery


os.environ.setdefault('CELERY_CONFIG_MODULE', 'backend.celery')

app = Celery('fusiondb_visualizer')
app.config_from_envvar('CELERY_CONFIG_MODULE')

app.conf.beat_schedule = {
    'cron': {
        'task': 'cron',
        'schedule': float(os.environ.get('BEAT_SCHEDULE', 300)),
        'args': (["beat"],)
    },
    'cleanup': {
        'task': 'cron',
        'schedule': float(os.environ.get('CLEANUP_SCHEDULE', 86400)),
        'args': (["cleanup"],)
    },
}

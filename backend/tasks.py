import os
import json
import shutil
import urllib.request
import urllib.parse
from datetime import datetime
from pathlib import Path
from .helpers import run_command
from .worker import app


HOSTNAME = os.getenv('HOSTNAME', 'python-backend')
APP_PATH = Path(os.getenv('APP_PATH', '/app'))
APP_SCRATCH = Path(os.getenv('APP_SCRATCH', '/scratch'))
KEEP_APP_SCRATCH_DAYS = int(os.getenv('KEEP_APP_SCRATCH_DAYS', 7))
APP_BIN = os.getenv('APP_BIN', 'bin/run_visualizer.sh')


@app.task(bind=True, name='cron', queue='beat')
def cron(self, tasks):  
    for task in tasks:
        print(f'got task: {task}')
        if task == 'cleanup':
            for sfolder in APP_SCRATCH.glob('*'):
                createTime = datetime.fromtimestamp(sfolder.stat().st_atime)
                delta = datetime.now() - createTime
                if delta.days > KEEP_APP_SCRATCH_DAYS:
                    shutil.rmtree(sfolder)
        elif task == 'beat':
            print(f'Got beat: {HOSTNAME}')


@app.task(bind=True, name='visualizer.submit')  
def submit(self, *args, **kwargs):
    
    # process taks
    print(f'New visualizer submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['query']

    scratch_folder = APP_SCRATCH / kwargs['_token']
    os.umask(0)
    scratch_folder.mkdir(mode=0o777, parents=True, exist_ok=True)

    run_command(
        [
            APP_BIN,
            kwargs['query'],
            kwargs['type'],
            scratch_folder / kwargs['input'],
            scratch_folder / kwargs['output'],
            kwargs['column'],
            kwargs['layout'],
            kwargs['subnet'],
            kwargs['nodescale'],
            kwargs['edgescale'],
            kwargs['optional']
        ]
    )

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')

    if kwargs['optional'] == '-f':
        scope = 'full'
    elif kwargs['optional'] == '-b':
        scope = 'subnet'
    else:
        scope = 'subnet'
    files = {
        'legend': '.'.join([kwargs['output'], 'legend', scope, 'csv']),
        'gexf': '.'.join([kwargs['output'], scope, 'gexf']),
        'gml': '.'.join([kwargs['output'], scope, 'gml']),
        'png': '.'.join([kwargs['output'], scope, 'png']),
        'pdf': '.'.join([kwargs['output'], scope, 'pdf']),
        'svg': '.'.join([kwargs['output'], scope, 'svg'])
    }

    return json.dumps(files)
